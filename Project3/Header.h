#pragma once

struct Element {
	int _value;
	int _row_position;
	int _column_position;

	Element(int val, int posI, int posJ) {
		_value = val;
		_row_position = posI;
		_column_position = posJ;
	}
	Element(Element &obj) {
		_value = obj._value;
		_row_position = obj._row_position;
		_column_position = obj._column_position;
	}
	Element() {
		_value = -10000;
		_row_position = -1;
		_column_position = -1;
	}
	Element& operator=(const Element& obj) {
		_value = obj._value;
		_row_position = obj._row_position;
		_column_position = obj._column_position;

		return *this;
	}
};

int choiseTask(int& task, int** Matrix, int rowCount, int columnCount);
Element MinimalElementOnDiagonal(int** Matrix, int rowCount, int columnCount);
Element MinimalElementUnderDiagonal(int** Matrix, int rowCount, int columnCount);
void ReplaceElementOnDiagonal(int** Matrix, int rowCount, int columnCount);
void printMatrix(int** Matrix, int rowCount, int columnCount);
int** createMatrix(int rowCount, int columnCount);

Element MaximalElementOnDiagonal(int** Matrix, int rowCount, int columnCount);
int GetSumOverDiagonal(int** Matrix, int rowCount, int columnCount);
void ReplaceBlocksInMatrix(int** Matrix, int rowCount, int columnCount);
void GetSumOfMatrices(int** Matrix, int rowCount, int columnCount);