#include <iostream>
#include <iomanip>
#include "Header.h"

using namespace std;

int choiseTask(int& task, int** Matrix, int rowCount, int columnCount)
{
	setlocale(LC_ALL, "Russian");
	task = 0;
	system("cls");
	cout << "�������: \n";
	printMatrix(Matrix, rowCount, columnCount);
	cout << "�������� ��������:\n";
	cout << "1-����� ����������� �� ���������, ������������� �� �������� ���������\n";
	cout << "2-����� ����������� �� ���������, ������������� ���� �������� ���������\n";
	cout << "3-�������� ����������� �� ���������, ������������� �� �������� ���������, �� ����������� �� ���������, ������������� ���� �������� ���������\n";
	cout << "4-����� ������������ �� ���������, ������������� �� �������� ���������\n";
	cout << "5-����� ����� ���������, ������������� ��� �������� ���������\n";
	cout << "6-���������� ������������ ������\n";
	cout << "7-�������� �������������� ������� � ���������� �� ��������\n";
	cout << "8-�����\n>> ";
	cin >> task;
	if ((task < 1) && (task > 8))
	{
		cout << "������������ ����\n\n";
		system("pause");
		choiseTask(task, Matrix, rowCount, columnCount);
	}
	if ((task >= 1) && (task <= 7))
	{
		return 1;
		system("cls");
	}
	if (task == 8)
	{
		task = -1;
	}
}

Element MinimalElementOnDiagonal(int** Matrix, int rowCount, int columnCount)
{
	int x = columnCount - 1;
	int MinimalElementOnDiagonal = Matrix[0][columnCount - 1];
	int positionI = 0, positionJ = columnCount - 1;
	for (int i = 0; i < rowCount; i++)
	{
		if (Matrix[i][x] < MinimalElementOnDiagonal)
		{
			MinimalElementOnDiagonal = Matrix[i][x];
			positionI = i;
			positionJ = x;
		}
		x--;
	}
	Element El(MinimalElementOnDiagonal, positionI, positionJ);
	return El;
}

Element MaximalElementOnDiagonal(int** Matrix, int rowCount, int columnCount)
{
	int x = columnCount - 1;
	int MaximalElementOnDiagonal = Matrix[0][columnCount - 1];
	int positionI = 0, positionJ = columnCount - 1;
	for (int i = 0; i < rowCount; i++)
	{
		if (Matrix[i][x] > MaximalElementOnDiagonal)
		{
			MaximalElementOnDiagonal = Matrix[i][x];
			positionI = i;
			positionJ = x;
		}
		x--;
	}
	Element El(MaximalElementOnDiagonal, positionI, positionJ);
	return El;
}

Element MinimalElementUnderDiagonal(int** Matrix, int rowCount, int columnCount)
{
	int x = columnCount - 1;
	int MinimalElementUnderDiagonal = Matrix[rowCount - 1][columnCount - 1];
	int positionI = rowCount - 1, positionJ = columnCount - 1;
	int i{}, j{};
	for (i = 0; i < rowCount; i++)
	{
		for (j = x; j < columnCount; j++)
		{
			if (j > x)
			{
				if (Matrix[i][j] < MinimalElementUnderDiagonal)
				{
					MinimalElementUnderDiagonal = Matrix[i][j];
					positionI = i;
					positionJ = j;
				}
			}
		}
		x--;
	}
	Element El(MinimalElementUnderDiagonal, positionI, positionJ);
	return El;
}

void ReplaceElementOnDiagonal(int** Matrix, int rowCount, int columnCount)
{
	int** NewMatrix{};
	NewMatrix = new int*[rowCount];
	for (int i = 0; i < rowCount; i++)
	{
		NewMatrix[i] = new int[columnCount];
	}

	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			NewMatrix[i][j] = 0;
		}
	}

	Element On;
	On = MinimalElementOnDiagonal(Matrix, rowCount, columnCount);
	Element Under;
	Under = MinimalElementUnderDiagonal(Matrix, rowCount, columnCount);
	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			NewMatrix[i][j] = Matrix[i][j];
		}
	}
	NewMatrix[On._row_position][On._column_position] = Under._value;
	cout << "���������� �������: \n";
	printMatrix(NewMatrix, rowCount, columnCount);
	delete[] NewMatrix;
}

int GetSumOverDiagonal(int** Matrix, int rowCount, int columnCount)
{
	int x = columnCount - 1;
	int sum{};
	int positionI = rowCount - 1, positionJ = columnCount - 1;
	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < x; j++)
		{
			sum += Matrix[i][j];
		}
		x--;
	}
	return sum;
}

void ReplaceBlocksInMatrix(int** Matrix, int rowCount, int columnCount)
{
	int** NewMatrix{};
	NewMatrix = new int*[rowCount];
	for (int i = 0; i < rowCount; i++)
	{
		NewMatrix[i] = new int[columnCount];
	}
	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			NewMatrix[i][j] = Matrix[i][j];
		}
	}

	NewMatrix[0][0] = Matrix[1][0];
	NewMatrix[1][0] = Matrix[0][0];
	printMatrix(NewMatrix, rowCount, columnCount);
	delete[] NewMatrix;
}

void GetSumOfMatrices(int** Matrix, int rowCount, int columnCount)
{
	cout << "������ �������: \n";
	printMatrix(Matrix, rowCount, columnCount);
	int** SecondMatrix{};
	SecondMatrix = new int*[rowCount];
	for (int i = 0; i < rowCount; i++)
	{
		SecondMatrix[i] = new int[columnCount];
	}

	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			SecondMatrix[i][j] = rand() % (20 + 1) - 11;
		}
	}
	cout << "\n������ �������\n";
	printMatrix(SecondMatrix, rowCount, columnCount);

	int** Result{};
	Result = new int*[rowCount];
	for (int i = 0; i < rowCount; i++)
	{
		Result[i] = new int[columnCount];
	}

	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			Result[i][j] = Matrix[i][j] + SecondMatrix[i][j];
		}
	}
	cout << "\n��������� �������� ������: \n";
	printMatrix(Result, rowCount, columnCount);
	delete[] SecondMatrix;
	delete[] Result;
}

void printMatrix(int** Matrix, int rowCount, int columnCount)
{
	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			cout << setw(4) << Matrix[i][j];
		}
		cout << '\n';
	}
}
int** createMatrix(int rowCount, int columnCount)
{
	//�������� ������������ �������
	int** Matrix{};
	Matrix = new int*[rowCount];
	for (int i = 0; i < rowCount; i++)
	{
		Matrix[i] = new int[columnCount];
	}
	//���������� �������
	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < columnCount; j++)
		{
			Matrix[i][j] = rand() % (20 + 1) - 11;
		}
	}
	return Matrix;
}