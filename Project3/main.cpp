#include <iostream>
#include <ctime>
#include "Header.h"

using namespace std;

void main()
{
	srand(time(0));
	setlocale(LC_ALL, "Russian");
	bool trigger = false;
	int task{};
	Element MinimalElementOnDiagonal_obj;
	Element MaximalElementOnDiagonal_obj;
	Element MinimalElementUnderDiagonal_obj;
	int rowCount = 3, columnCount = 3;
	int** Matrix = createMatrix(rowCount, columnCount);
	do
	{
		cout << '\n';
		choiseTask(task, Matrix, rowCount, columnCount);
		switch (task)
		{
		case 1:
			system("cls");
			MinimalElementOnDiagonal_obj = MinimalElementOnDiagonal(Matrix, rowCount, columnCount);
			cout << "����������� ������� �� ���������: " << MinimalElementOnDiagonal_obj._value;
			cout << "\n��� �������: " << MinimalElementOnDiagonal_obj._row_position + 1 << " " << MinimalElementOnDiagonal_obj._column_position + 1 << "\n\n";
			break;
		case 2:
			system("cls");
			MinimalElementUnderDiagonal_obj = MinimalElementUnderDiagonal(Matrix, rowCount, columnCount);
			cout << "����������� ������� ��� ����������: " << MinimalElementUnderDiagonal_obj._value;
			cout << "\n��� �������: " << MinimalElementUnderDiagonal_obj._row_position + 1 << " " << MinimalElementUnderDiagonal_obj._column_position + 1 << "\n\n";
			break;
		case 3:
			system("cls");
			ReplaceElementOnDiagonal(Matrix, rowCount, columnCount);
			break;
		case 4:
			system("cls");
			MaximalElementOnDiagonal_obj = MaximalElementOnDiagonal(Matrix, rowCount, columnCount);
			cout << "������������ ������� �� ���������: " << MaximalElementOnDiagonal_obj._value;
			cout << "\n��� �������: " << MaximalElementOnDiagonal_obj._row_position + 1 << " " << MaximalElementOnDiagonal_obj._column_position + 1 << "\n\n";
			break;
		case 5:
			system("cls");
			cout << "����� ��������� ��� ����������: " << GetSumOverDiagonal(Matrix, rowCount, columnCount) << "\n";
			break;
		case 6:
			system("cls");
			cout << "���������� �������: \n";
			ReplaceBlocksInMatrix(Matrix, rowCount, columnCount);
			break;
		case 7:
			system("cls");
			GetSumOfMatrices(Matrix, rowCount, columnCount);
			break;
		case -1:
			system("cls");
			trigger = true;
			break;
		}
		system("pause");
	} while (trigger == false);
	delete[] Matrix;
}